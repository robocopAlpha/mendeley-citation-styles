# Mendeley Citation Styles

Because it is impossible to retreive you have created/modified on [Mendeley’s website](https://csl.mendeley.com/) even if you log into your account or search with your name, I decided to host my `csl` files on gitlab for posterity.

This repository hosts a collection of my personalized mendeley citation styles, (also checked to work with Zotero). This is because I occasionally found the formatting for websites or book chapters to be a bit off in the default version of some of these following themes on Mendeley’s [reference styles](https://csl.mendeley.com/about/) portal (this information was true at the time when I “forked” the named styles to create my own.)

**Disclaimer**: These themes are not official themes and I am not affiliated with any of the mentioned publishing houses. These were my best interpretations and approximations of the reference formatting instructions provided by the indicated publishing houses.



## How to use:

Copy the URL for the ```raw``` format of the file → Paste that  in Mendeley’s ```Get More Styles``` dialog box. Here’s a short GIF showing the process.

![mendeley citation](img/mendeley citation.gif)

## Modified reference styles:

### 1. [American Association of Cancer research](american-association-for-cancer-research-2.csl)


Formatting used by most AACR group of journals.

![AACR](img/AACR.png)

______________________________

### 2. [Biomed Central](biomed-central.csl)

Formatting used by most of BMC group of journals.

![Biomed Central](img/Biomed Central.png)

_________________

### 3. [Harvard Reference Format](harvard1-DC.csl)

![Harvard](img/Harvard.png)

__________________

### 4. [Nature Breif Communications](nature-breif-comm.csl)

Mendeley has a great and regularly updated style for Nature group of journals. This was my attempt at making a more compact citation that resembles Nature breif communications. Very useful for using in grant proposals or research plans where there is a page limit.

![Nature Breif comm](img/Nature Breif comm.png)

_____________

### 5. [Poster-DC](Poster-DC.csl)

A citation style I created specifically to use with my posters. It is compact, yet has all the essential info.

![Poster](img/Poster.png)

_____________

### 6. [Vancouver style](vancouver-dc)

![Vancouver](img/Vancouver.png)

